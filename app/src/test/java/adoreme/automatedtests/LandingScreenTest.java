package adoreme.automatedtests;

import junit.framework.Assert;

import org.junit.Test;

/**
 * Created by catalinvasile on 22/12/16.
 */

public class LandingScreenTest extends BaseTest {

    //@Test
    public void loginWithSmartLock() throws Exception {
        app.landingScreen().loginWithSmartlock();
    }

    @Test
    public void loginWithValidCredentials() throws Exception {
        app.landingScreen().login("test115@gmail.com", "parola123");
        Assert.assertTrue(app.homeScreen().isActive());
    }

   // @Test
    public void loginWithInvalidCredentials() throws Exception {
        app.landingScreen().login("test115@gmail.com", "parola1234");
        Assert.assertTrue(app.landingScreen().isErrorMessageShown());
    }

    //@Test
    public void signupWithValidCredentials() throws Exception {
    }

    //@Test
    public void signupWithInvalidCredentials() throws Exception {

    }

    //@Test
    public void authenticateWithFacebook() throws Exception {

    }


}
