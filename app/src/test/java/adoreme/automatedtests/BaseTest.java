package adoreme.automatedtests;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

/**
 * Created by catalinvasile on 26/12/16.
 */

public abstract class BaseTest {

    protected AppiumDriver driver;
    protected App app;


    @Before
    public void setup() throws Exception {

        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "myNexus5x");
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 4000);
        cap.setCapability(MobileCapabilityType.APP, "/Users/catalinvasile/Desktop/4.7.6_production.apk");
        driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        app = new App(driver);
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
