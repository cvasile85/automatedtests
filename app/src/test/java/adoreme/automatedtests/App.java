package adoreme.automatedtests;

import adoreme.automatedtests.screens.HomeScreen;
import adoreme.automatedtests.screens.LandingScreen;
import io.appium.java_client.AppiumDriver;

/**
 * Created by catalinvasile on 26/12/16.
 */

public class App {

    private AppiumDriver driver;

    public App(AppiumDriver driver) {
        this.driver = driver;
    }

    public LandingScreen landingScreen() {
        return new LandingScreen(driver);
    }

    public HomeScreen homeScreen() {
        return new HomeScreen(driver);
    }
}
