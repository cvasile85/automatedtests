package adoreme.automatedtests.utils;

/**
 * Created by catalinvasile on 26/12/16.
 */

public class Constants {

    public static final int TIMEOUT_IN_SECONDS = 5;
    public static final String INVALID_CREDENTIALS_MESSAGE = "Invalid login or password.";
}
