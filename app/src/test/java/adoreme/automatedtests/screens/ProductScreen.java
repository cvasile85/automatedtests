package adoreme.automatedtests.screens;

import io.appium.java_client.AppiumDriver;

/**
 * Created by catalinvasile on 26/12/16.
 */

public class ProductScreen extends BaseScreen {
    public ProductScreen(AppiumDriver driver) {
        super(driver);
    }

    public void addProductInBag() {

    }

    public void addProductWithExtraPantyInBag() {

    }

    public void addProductToWishlist() {

    }

    public void removeProductFromWishlist() {

    }
}
