package adoreme.automatedtests.screens;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Created by catalinvasile on 26/12/16.
 */

public class HomeScreen extends BaseScreen {

    @AndroidFindBy(id = "com.adoreme.android:id/cartImageView")
    private MobileElement shoppingBagIcon;

    public HomeScreen(AppiumDriver driver) {
        super(driver);
    }

    public boolean isActive() {
        return new WebDriverWait(driver, 15).until(ExpectedConditions.visibilityOf(shoppingBagIcon)).isDisplayed();
    }
}
