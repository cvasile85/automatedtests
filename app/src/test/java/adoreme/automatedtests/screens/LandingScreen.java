package adoreme.automatedtests.screens;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import adoreme.automatedtests.utils.Constants;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Created by catalinvasile on 26/12/16.
 */

public class LandingScreen extends BaseScreen {


    @AndroidFindBy(id = "com.google.android.gms:id/credentials_picker_title")
    private MobileElement credentialsPicker;

    @AndroidFindBy(id = "com.google.android.gms:id/credentials_hint_picker_title")
    private MobileElement credentialsHintPicker;

    @AndroidFindBy(id = "com.google.android.gms:id/cancel")
    private MobileElement cancelButton;

    @AndroidFindBy(id = "com.adoreme.android:id/emailEditText")
    private MobileElement emailField;

    @AndroidFindBy(id = "com.adoreme.android:id/passwordEditText")
    private MobileElement passwordField;

    @AndroidFindBy(id = "com.adoreme.android:id/continueWithEmailButton")
    private MobileElement continueWithEmailButton;

    @AndroidFindBy(id = "com.adoreme.android:id/continueWithFacebookButton")
    private MobileElement continueWithFacebookButton;

    @AndroidFindBy(id = "com.adoreme.android:id/submitButton")
    private MobileElement submitButton;

    @AndroidFindBy(id = "android:id/message")
    private MobileElement message;

    public LandingScreen(AppiumDriver driver) {
        super(driver);
    }

    public void loginWithSmartlock() {

    }

    public void login(String email, String password) {

        dismissCredentialsPicker();

        //findElementWithTimeout(By.ById.id("com.adoreme.android:id/continueWithEmailButton"), 5).click();

        continueWithEmailButton.click();

        dismissCredentialsHintPicker();

        emailField.sendKeys(email);
        submitButton.click();

        passwordField.sendKeys(password);
        submitButton.click();
    }


    private void dismissCredentialsPicker() {

        if (credentialsPicker.isDisplayed()) {
            cancelButton.click();
        }
    }

    private void dismissCredentialsHintPicker() {
        if (credentialsHintPicker.isDisplayed()) {
            cancelButton.click();
        }
    }

    public boolean isErrorMessageShown() {
        return new WebDriverWait(driver, 15).until(ExpectedConditions.textToBePresentInElement(message, Constants.INVALID_CREDENTIALS_MESSAGE));
    }

}
